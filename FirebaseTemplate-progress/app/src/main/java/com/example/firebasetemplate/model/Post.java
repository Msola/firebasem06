package com.example.firebasetemplate.model;

import android.net.Uri;

import java.util.HashMap;

public class Post {
    public String postid;
    public String content;
    public String authorName;
    public String date;
    public String imageUrl;
    public String authorImageUrl;

    public HashMap<String, Boolean> likes = new HashMap<>();
}
