package com.example.firebasetemplate;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.databinding.FragmentSignOutBinding;
import com.example.firebasetemplate.databinding.FragmentViewPostBinding;
import com.example.firebasetemplate.model.Comment;
import com.example.firebasetemplate.model.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class ViewPostFragment extends AppFragment {


    private FragmentViewPostBinding binding;

    public ViewPostFragment() { }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (binding = FragmentViewPostBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String postid = ViewPostFragmentArgs.fromBundle(getArguments()).getPostid();

        if(!postid.equals("") || postid!=null) {
            loadComments(postid);

            //ViewPostFragmentArgs.fromBundle(getArguments()).getPostid();
            db.collection("posts").document(postid).addSnapshotListener((value, error) -> {
                Post postData = value.toObject(Post.class);

                binding.postContent.setText(postData.content);
                binding.postAuthorName.setText(postData.authorName);
                binding.postDate.setText(postData.date);
                binding.postLikes.setText("Likes: " + postData.likes.size());

                Glide.with(requireContext()).load(postData.authorImageUrl).circleCrop().into(binding.authorPostPhoto);
                Glide.with(requireContext()).load(postData.imageUrl).fitCenter().into(binding.postPhotoBig);

            });

            binding.buttonComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String newComment = binding.commentLayout.getEditText().getText().toString();
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    if (!newComment.equals("")) {

                        Comment comment = new Comment();
                        comment.commentText = newComment;
                        comment.authorName = FirebaseAuth.getInstance().getCurrentUser().getEmail();
                        comment.date = LocalDateTime.now().toString();
                        comment.commentid = db.collection("comments").document().getId();
                        comment.authorImageUrl = FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString();

                        db.collection("posts").document(postid)
                                .collection("comments").document(comment.commentid).set(comment)
                                .addOnSuccessListener(task -> {
                                    Toast.makeText(getContext(), "Comment Published", Toast.LENGTH_SHORT).show();
                                    loadComments(postid);
                                });
                    }
                }
            });

        }

    }

    private void loadComments(String postid){
        //Fragment father = requireContext();
        List<Comment> comments = new ArrayList<>();
        db.collection("posts").document(postid).
                collection("comments").get().addOnCompleteListener((new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                /*db.collection("posts").document(postid)
                                        .collection("comments").document(document.getId())
                                        .addSnapshotListener((value, error) ->{
                                            Comment commentData = value.toObject(Comment.class);
                                            comments.add(commentData);

                                            CommentAdapter myAdapter = new CommentAdapter(comments, getContext(), father);
                                            binding.recyclerComments.setAdapter(myAdapter);
                                            binding.recyclerComments.setLayoutManager(new LinearLayoutManager(getContext()));
                                        });*/
                                Comment commentTmp = document.toObject(Comment.class);
                                comments.add(commentTmp);
                                CommentAdapter myAdapter = new CommentAdapter(comments, getContext());
                                binding.recyclerComments.setAdapter(myAdapter);
                                binding.recyclerComments.setLayoutManager(new LinearLayoutManager(getContext()));
                            }
                        }
                    }
                }));

        //return comments;
    }
}