package com.example.firebasetemplate;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.firebasetemplate.model.Comment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
    private List<Comment> mComments;
    private Context mContext;
   // private Fragment father;

    public CommentAdapter(List<Comment> mComments, Context mContext) {
        this.mComments = mComments;
        this.mContext = mContext;
        //this.father = father;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.comment_row, parent, false );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        /*Picasso.get().load(mComments.get(position).authorImageUrl)
                .fit()
                .centerCrop()
                .into(holder.imageCommentAuthor);*/
        Glide.with(mContext).load(mComments.get(position).authorImageUrl).circleCrop().into(holder.imageCommentAuthor);
        holder.commentAuthor.setText(mComments.get(position).authorName);
        holder.commentDate.setText(mComments.get(position).date);
        holder.commentText.setText(mComments.get(position).commentText);
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageCommentAuthor;
        private TextView commentAuthor;
        private TextView commentDate;
        private TextView commentText;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageCommentAuthor = itemView.findViewById(R.id.imageCommentAuthor);
            commentAuthor = itemView.findViewById(R.id.commentAuthor);
            commentDate = itemView.findViewById(R.id.commentDate);
            commentText = itemView.findViewById(R.id.commenntText);
        }
    }
}
