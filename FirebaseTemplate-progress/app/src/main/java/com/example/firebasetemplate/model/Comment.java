package com.example.firebasetemplate.model;

import java.util.HashMap;

public class Comment {
    public String commentid;
    public String commentText;
    public String authorName;
    public String date;
    public String authorImageUrl;


    public HashMap<String, Boolean> likes = new HashMap<>();
}
